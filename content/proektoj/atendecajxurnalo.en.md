---
title: "Waiting log"
date: 2023-02-18T19:26:06+08:00
draft: false
tags:
- rust
- author's
- web
---

In 2020, covid began, and the hospital where I worked changed its mode of operation, for this a kind of “waiting log” was needed, where a person was entered in order to know who to receive (or call back) when.

The project was written on the "Rocket" framework, but due to the fact that it used unstable growth - I decided not to use it anymore and I already wrote the following projects on the actix-web framework

![img](/proektoj/atendecajxurnalo.jpg)

## Used:
- rocket
- postgres
- tera
- rust

## Links:

[Repo](https://gitlab.com/JimGrendpot/record-calendar)