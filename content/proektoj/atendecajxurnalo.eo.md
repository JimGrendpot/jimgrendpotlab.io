---
title: "Atendĵurnalo"
date: 2023-02-18T19:26:06+08:00
draft: false
tags:
- rust
- Aŭtora    
- retejo
---

En 20 jaro staris kovid, kaj hospitalo, kie mi laboris, ŝangis sia aborreĝimo. Por tio ni bezonis "Atendĵurnalo", kie ili enskribis personon, por scii kion kaj kiam telefoni.

La proekto faris sur kadrlaboro "Rocket", sed ĉar ĝi uzis malstabla rust - mi decidis ne uzas tion kaj en sekvaj proektoj mi uzis "actix-web"

![img](/proektoj/atendecajxurnalo.jpg)

## Uziĝis:
- rocket
- postgres
- tera
- rust

## Retligoj:

[Proektulo](https://gitlab.com/JimGrendpot/record-calendar)