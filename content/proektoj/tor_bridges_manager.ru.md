---
title: "Управленец мостов тора"
date: 2023-02-18T19:54:30+08:00
draft: false
tags:
- rust
- авторское
- gui
---
На компьютере я пользовался тором как прокси, а для этого мне частенько приходилось менять мосты для тора.

Я написал библиотеку и программку для работы с мостами тора.

![скриншот программы](/proektoj/tor_bridges_manager.jpg)

## Использовалось:
- gtk
- rust

Верстал программу на Glade.

## Ссылки

[Репозиторий библиотеки](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager)

[Репозиторий с программой](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager_gui)