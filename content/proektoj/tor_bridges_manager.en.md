---
title: "Tor bridge manager"
date: 2023-02-18T19:54:30+08:00
draft: false
tags:
- rust
- author's
- gui
---
On a computer I use Tor as a Proxy, but for that I often need to change the bridge for Tor.

I wrote a library and a program to work with tor bridges.

![program window](/proektoj/tor_bridges_manager.jpg)

## Used:
- gtk
- rust

For layout I used "Glade".

## Links:

[Lib repo](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager)

[Prorgam repo](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager_gui)