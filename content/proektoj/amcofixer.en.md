---
title: "Amcofixer"
date: 2024-05-16T21:17:59+02:00
draft: false
tags: ["Rust"]
---

So, you know, that in Russia very hard censorship. And I try to wrote instrument, that will be helpfull for people in Russia.

It's not a hard project, and I try to make it without .unwrap() using error handler, Options<> and Results<>.

With the transition to a new version of amnesia, compatibility with the configs of older versions was lost. Often, small file conversions are enough to restore compatibility.

But since everything turned out that the Amnezia developers did not provide the ability to import or edit a separate config, and the QT developers have long ago abandoned God and are creating bicycles like qComporess.

So I just made this world a little better and made a program that fixes these configs. This does not work in one hundred percent of cases and the origin of many errors is not clear to me.

How it's work

Program

- Reads the config
- translates it to BASE64-URL
- DELETES the first 4 bytes (Lol)
- Using zlib unpacks the file
- Finds the desired value, edits

And then everything goes in the opposite direction.
