---
title: "Cartrige manager"
date: 2023-02-18T19:45:44+08:00
draft: false
tags:
- rust
- web
- actix-web
---
Program for cartridge management. You can add information about the last printer cartridge refill, cartridge status, and destination (district, address, etc.).

I wrote this program using actix-web. And now I understand that I can update some features. And yes. When I wrote this program, I didn't know about ORM. And I wrote queries to the database manually.

![](/proektoj/cartman.jpg)

And developers of Actix-web has position, that they framework during web-standarts. And I had some problem with multipart becuse it (Becuse web-standarts don't support this features).

And later i'm understand, that I should make frontend and beckend in devided parts, and connect they using API. 

## Used:
- rust
- actix-web
- postgres
- tera
- bootstrap

## Links

[Project repo](https://gitlab.com/tyap-lyap-ivprod/cartrigemanager)
