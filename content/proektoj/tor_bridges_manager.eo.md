---
title: "Pontoj de tor manaĝero"
date: 2023-02-18T19:54:30+08:00
draft: false
tags:
- rust
- aŭtora
- gui
---
Sur komputilo mi uzas Tor kial Proxy, sed por tion mi bezonas ofte ŝangis ponton por tor.

Mi skribis bibliotekon kaj programo por laboro kun pontoj de tor.

![ekrankopio de programo](/proektoj/tor_bridges_manager.jpg)

## Uziĝis:
- gtk
- rust

Por enpaĝigo mi uzis "Glade".

## Retligoj:

[Proektulo de biblioteko](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager)

[Proektulo de programo](https://gitlab.com/tyap-lyap-ivprod/tor_bridges_manager_gui)