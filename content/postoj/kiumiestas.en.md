---
title: "About me"
date: 2023-02-18T20:48:34+08:00
draft: false
weight: 1
---
I am a programmist from Siberia.

I prefer to write on rust language, but I can write on Python (for example on Django).

I have been doing this for a great four years and some of my projects are currently being used in hospitals.