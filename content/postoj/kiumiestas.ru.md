---
title: "Кто я???"
date: 2023-02-18T20:48:34+08:00
draft: false
weight: 1
---
Я программист из Сибири.

Предпочитаю писать на расте, но могу писать на питоне (к примеру на джанге).

Занимаюсь этим уже больше четырёх лет и несколько проектов прямо сейчас используются в учреждении.

Кроме написания программ я увлекаюсь прикладным шифропанком: шифровка информации, анонимность, защита устройств и т.д. Участвовал в проведении мероприятий по инфобезу (рассказывал про ОС Тэилс и про общую "цифровую гигиену" в условиях России двадцатых годов).

В этом блоге я буду писать про мои игры с растом и про некоторые инфобезовские инструменты.