---
title: "Kiu mi estas???"
date: 2023-02-18T20:48:34+08:00
draft: false
weight: 1
---
Mi estas programisto el Siberia.

Mi preferas skribi sur rust lingvo, sed mi povas skribi sur python (ekzemple sur django).

Mi faras tion jam grande kvar jaroj kaj iu miaj proektoj nune usiĝas en hospitaloj. 

Krom tio - mi hobias aplikan ĉifropunkon: informĉifro, anonimeco, aparata defendo, ktp. Mi partopreno en organizado de informeventajn sekurecojn (OC "Tails" kaj ĉifra higieno en Rusa realo de 20-j jaroj).

En tio blogo mi volas skribi pro mia ludo kun rust ligvo kaj iom informeventajn sekurecajn laborilojn.